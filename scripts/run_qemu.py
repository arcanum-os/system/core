#!/usr/bin/env python
import subprocess
import argparse
import os, os.path
import glob
import sys
import tempfile
from shutil import which, copyfile

assert "PROJECT_DIR" in os.environ
assert "PROJECT_NAME" in os.environ
assert "BUILD_DIR" in os.environ
assert "BUILD_ARCH" in os.environ

PROJECT_DIR = os.environ["PROJECT_DIR"]
PROJECT_NAME = os.environ["PROJECT_NAME"]
BUILD_DIR = os.environ["BUILD_DIR"]
BUILD_ARCH = os.environ["BUILD_ARCH"]

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="runs the specified image in qemu.")
	parser.add_argument("-a", "--arch", required=False, help="target architecture to run")
	parser.add_argument("-i", "--image", required=False, help="the iso image to run in qemu")

	args = parser.parse_args()

	if args.arch is None:
		args.arch = BUILD_ARCH
	
	if args.image is None:
		args.image = f"{BUILD_DIR}/{PROJECT_NAME}.iso"

	print(args.image)
	assert os.path.isfile(args.image)

	qemu = "qemu-system-"

	match args.arch:
		case "x86":
			qemu = qemu + "i386"
		case _:
			qemu = qemu + args.arch

	if args.image == None or not os.path.isfile(args.image):
		print("!! no iso image found:", args.image)
		sys.exit(1)

	if which(qemu) == None:
		print("!! qemu not installed:", qemu)
		sys.exit(2)

	p = subprocess.Popen(f"{qemu} \
		-s -S \
		-m 512 \
		-d int \
		-chardev stdio,id=char0,logfile=serial.log,signal=off \
		-serial chardev:char0 \
		-cdrom {args.image}",
		shell = True)
	print(">>", p.args)
	
	if p.wait() != 0:
		sys.exit(p.returncode)
