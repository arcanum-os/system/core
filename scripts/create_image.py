#!/usr/bin/env python
import subprocess
import argparse
import os, os.path
import glob
import sys
import tempfile
from shutil import which, copyfile

assert "PROJECT_DIR" in os.environ
assert "PROJECT_NAME" in os.environ
assert "BUILD_DIR" in os.environ

MKRESCUE = None

if which("grub2-mkrescue") != None:
	MKRESCUE = "grub2-mkrescue"
elif which("grub-mkrescue") != None:
	MKRESCUE = "grub-mkrescue"

assert MKRESCUE != None

PROJECT_DIR = os.environ["PROJECT_DIR"]
PROJECT_NAME = os.environ["PROJECT_NAME"]
BUILD_DIR = os.environ["BUILD_DIR"]

print(BUILD_DIR)
print(PROJECT_NAME)

GRUB_CFG = """
set timeout=3
set default=0

menuentry \"live\" {
	multiboot2 /core
	boot
}
"""

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="creates a bootable grub iso for the kernel.")
	parser.add_argument("-k", "--kernel", required=False, help="the kernel binary to package in the iso")

	args = parser.parse_args()

	if args.kernel is None:
		args.kernel = f"{BUILD_DIR}/{PROJECT_NAME}"

	print(args.kernel)
	assert os.path.isfile(args.kernel)

	output = f"{BUILD_DIR}/{PROJECT_NAME}.iso"
	isoroot = tempfile.TemporaryDirectory()
	
	print("|| iso root:", isoroot.name)

	os.makedirs(f"{isoroot.name}/boot/grub")
	
	with open(f"{isoroot.name}/boot/grub/grub.cfg", "w+") as f:
		f.write(GRUB_CFG)
	
	copyfile(args.kernel, f"{isoroot.name}/core")

	p = subprocess.Popen(f"{MKRESCUE} -o {output} {isoroot.name}",
		shell = True)
	print(">>", p.args)

	if p.wait() != 0:
		isoroot.cleanup()
		sys.exit(p.returncode)
	
	isoroot.cleanup()
