import os

assert "SEARCH_DIR" in os.environ
assert "SEARCH_EXT" in os.environ

SEARCH_DIR = os.environ["SEARCH_DIR"]
SEARCH_EXT = os.environ["SEARCH_EXT"]

for root, dirs, files in os.walk(os.path.abspath(SEARCH_DIR)):
	for file in files:
		if file.endswith(SEARCH_EXT):
			print(os.path.abspath(os.path.join(root, file)))
