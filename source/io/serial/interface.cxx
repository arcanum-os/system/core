/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "serial.hxx"

namespace inanis::io::serial {

	Interface::Interface() { }

	Result<Empty, Error> Interface::connect(Port port, u32 baudrate) {
		if(baudrate <= 0 || baudrate > BASE_BAUD_RATE) {
			Error e = ERROR(0, "invalid baudrate");
			return FAILURE(Empty, Error, &e);
		}

		output<u8>(port + 1, 0x00); /* disable interrupts */
		output<u8>(port + 3, 0x80); /* enable DLAB */
		output<u8>(port + 0, BASE_BAUD_RATE / baudrate); /* set baud rate divisor */
		output<u8>(port + 1, 0x00);
		output<u8>(port + 3, 0x03); /* 8 bits, no parity, one stop bit */
		output<u8>(port + 2, 0xC7); /* FIFO */
		output<u8>(port + 4, 0x0B); /* IRQs enabledm RTS/DSR set */
		output<u8>(port + 4, 0x1E); /* loopback mode */
		output<u8>(port + 0, 0x66); /* test byte */

		byte res = io::input<u8>(port);

		if(res != 0x66) {
			Error e = ERROR(0, "invalid baudrate");
			return FAILURE(Empty, Error, &e);
		}

		output<u8>(port + 4, 0x0F); /* normal mode */

		this->port = port;
		this->baudrate = baudrate;

		return SUCCESS(Empty, Error, nullptr);
	}

	bool Interface::is_available() {
		if(baudrate <= 0) return false;
		return (io::input<u8>(port + 5) & 1) != 0;
	}

	bool Interface::is_queue_empty() {
		if(baudrate <= 0) return false;
		return (io::input<u8>(port + 5) & 0x20) != 0;
	}

	void Interface::set_blocking(bool blocking) {
		this->blocking = blocking;
	}

	byte Interface::read() {
		if(baudrate <= 0) return 0;

		if(blocking) {
			while(!is_available());
		}

		return io::input<u8>(port);
	}

	byte Interface::write(byte value) {
		if(baudrate <= 0) return 0;

		if(blocking) {
			while(!is_queue_empty());
		}

		output<u8>(port, value);
		return value;
	}
}
