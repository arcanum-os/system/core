/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <types.h>
#include <xstd/result.hxx>
#include <xstd/error.hxx>
#include <logging/reader.hxx>
#include <logging/writer.hxx>
#include <io/io.hxx>

namespace inanis::io::serial {
	
	const u32 BASE_BAUD_RATE = 115200;

	enum Port : u32 {
		COM1 = 0x3F8,
		COM2 = 0x2F8,
		COM3 = 0x3E8,
		COM4 = 0x2E8,
		COM5 = 0x5F8,
		COM6 = 0x4F8,
		COM7 = 0x5E8,
		COM8 = 0x4E8,
	};

	class Interface : public Reader, public Writer {
	private:
		bool blocking = true;
	public:
		Port port;
		u32 baudrate;

		Interface();

		Result<Empty, Error> connect(Port port, u32 baudrate);

		bool is_available();
		bool is_queue_empty();

		void set_blocking(bool blocking);

		byte read();
		byte write(byte value);
	};
}
