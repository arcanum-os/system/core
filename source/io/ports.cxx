/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "io.hxx"

#define PORT_INPUT(x) asm volatile(x \
	: "=a" (result) \
	: "Nd" (port) \
	: "memory")

#define PORT_OUTPUT(x) asm volatile(x \
	: \
	: "a" (value), "Nd" (port) \
	: "memory");

namespace inanis::io {

	template<typename T>
	T input(u16 port) {
		T result;

		switch(sizeof(T)) {
			case 1:
				PORT_INPUT("inb %1, %%al");
				break;
			case 2:
				PORT_INPUT("inw %1, %%ax");
				break;
			case 4:
				PORT_INPUT("inl %1, %%eax");
				break;
		}

		return result;
	}

	template<typename T>
	void output(u16 port, T value) {
		switch(sizeof(T)) {
			case 1:
				PORT_OUTPUT("outb %%al, %1");
				break;
			case 2:
				PORT_OUTPUT("outw %%ax, %1");
				break;
			case 4:
				PORT_OUTPUT("outl %%eax, %1");
				break;
		}
	}

	template u8 input<u8>(u16 port);
	template u16 input<u16>(u16 port);
	template u32 input<u32>(u16 port);

	template void output<u8>(u16 port, u8 value);
	template void output<u16>(u16 port, u16 value);
	template void output<u32>(u16 port, u32 value);
}
