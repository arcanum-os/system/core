/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <types.h>

namespace inanis::io {

	template<typename T>
	T input(u16 port);

	template<typename T>
	void output(u16 port, T value);
}
