/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <stdint.h>
#include <panic.hxx>

extern "C" {
	uintptr_t __stack_chk_guard = 0xDEADC0DE;

	__attribute__((weak,noreturn)) void __stack_chk_fail(void) {
		PANIC("stack protector check failed");
	}
}
