/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <panic.hxx>

namespace __cxxabiv1 {

	__extension__ typedef int __guard __attribute__((mode(__DI__)));

	extern "C" int __cxa_guard_acquire(__guard *);
	extern "C" void __cxa_guard_release(__guard *);
	extern "C" void __cxa_guard_abort(__guard *);

	extern "C" int __cxa_guard_acquire(__guard *g) {
		return !*(char *)(g);
	}

	extern "C" void __cxa_guard_release(__guard *g) {
		*(char*) g = 1;
	}

	extern "C" void __cxa_guard_abort(__guard *) {
		PANIC("cxa guard check failed");
	}
}
