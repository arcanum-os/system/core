/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "logging.hxx"
#include <stdio.h>

void putchar_(char c) {
	inanis::logging::STDOUT->write(c);
}

namespace inanis::logging {
	
	Writer* STDOUT = nullptr;

	void log(Level level, const char* source, const char* message) {
#ifndef __DEBUG__
		if(level == Level::DEBUG) return;
#endif

		switch(level) {
			case Level::DEBUG:
				printf("[DEBUG]");
				break;
			case Level::INFO:
				printf("[INFO]");
				break;
			case Level::WARN:
				printf("[WARNING]");
				break;
			case Level::ERROR:
				printf("[ERROR]");
				break;
			case Level::CRIT:
				printf("[CRITICAL]");
				break;
			case Level::FATAL:
				printf("[FATAL]");
				break;
		}

		printf(" (%s) %s\n", source, message);
	}
}
