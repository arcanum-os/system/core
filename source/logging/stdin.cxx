/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "logging.hxx"
#include <string.h>
#include <stdio.h>

#define INPUT_BUFFER_SIZE 1024
#define ENDLINE_CHAR '\r'

Reader* inanis::logging::STDIN = nullptr;

char* input(const char* prompt) {
	static char buffer[INPUT_BUFFER_SIZE];
	char next;

	int i = 0;

	printf("%s", prompt);

	while((next = inanis::logging::STDIN->read()) != ENDLINE_CHAR) {
		buffer[i] = next;
		i++;

		putchar(next);
	}

	printf("\n");

	buffer[i] = '\0';
	return buffer;
}
