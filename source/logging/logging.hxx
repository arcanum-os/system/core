/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <types.h>
#include <string.h>
#include "reader.hxx"
#include "writer.hxx"

char* input(const char* prompt);

#define LOG(level, msg) inanis::logging::log( \
		inanis::logging::Level::level, \
		__FILE_NAME__ STRINGIFY(:__LINE__), \
		msg \
	)

#define _LOG(level, source, msg) inanis::logging::log( \
		inanis::logging::Level::level, \
		source, \
		msg \
	)

namespace inanis::logging {
	
	extern Reader* STDIN;
	extern Writer* STDOUT;

	enum Level : size_t {
		DEBUG,
		INFO,
		WARN,
		ERROR,
		CRIT,
		FATAL,
	};

	void log(Level level, const char* source, const char* message);
}
