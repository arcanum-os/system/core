/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once
#include <string.h>
#include <stdio.h>
#include <logging/logging.hxx>

#define PANIC(msg) panic(panic_frame { \
		.source = __FILE__ STRINGIFY(:__LINE__), \
		.message = msg \
	})

struct panic_frame {
	const char* source;
	const char* message;
};

__attribute__((noreturn))
static void panic(panic_frame frame) {
	LOG(FATAL, "kernel panic!");
	printf("at: %s\n\n", frame.source);
	printf("%s", frame.message);

	while(true) {
		asm volatile("cli; hlt");
	}
}
