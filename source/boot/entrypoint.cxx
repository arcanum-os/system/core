/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include <config.h>
#include <arch.hxx>
#include <io/serial/serial.hxx>
#include <logging/logging.hxx>
#include <types.h>
#include <printf/printf.h>
#include <panic.hxx>
#include <boot/boot.hxx>

using namespace inanis;
using namespace inanis::io;

extern "C" void _start1(bootinfo* info) {
	static serial::Interface si;

	if(si.connect(serial::Port::COM1, 115200).is_err()) {
		return; /* no way to panic if we don't have STDOUT */
	}

	logging::STDOUT = &si;
	logging::STDIN = &si;

	LOG(INFO, "testing printf library");
	printf_("Hello, %s! %d\n", "world", 42);
	printf_("%p\n", (void*) &si);

	LOG(INFO, "initializing architecture-specific code...");
	arch::initialize();
	LOG(INFO, "...OK");

	LOG(DEBUG, "testing CPU exception handling (page fault)");

	u32* ptr = (u32*) 0x12345;
	*ptr = 10;
}
