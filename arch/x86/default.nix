#!/usr/bin/env nix-shell
{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/ca012a02bf8327be9e488546faecae5e05d7d749.tar.gz") { } }:
pkgs.mkShell {
	nativeBuildInputs = with pkgs.buildPackages; [
		llvmPackages_16.clangNoLibcxx
		lld_16
		nasm

		ninja
		meson

		gdb
		xorriso
		grub2
		buildPackages.qemu
	];
}
