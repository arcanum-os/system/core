/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <types.h>
#include <cpu/gdt/segment/selector.hxx>

namespace inanis::arch::cpu::idt {

	enum GateType : u8 {
		TaskGate = 0b0101,
		InterruptGate16 = 0b0110,
		TrapGate16 = 0b0111,
		InterruptGate32 = 0b1110,
		TrapGate32 = 0b1111,
	};

	struct GateDescriptor {
		u16 offset_0 : 16;
		u16 selector : 16;
		u8 reserved : 8;
		u8 type : 4;
		bool zero : 1;
		u8 privilege_level : 2;
		bool present : 1;
		u16 offset_1 : 16;

		GateDescriptor() { }

		GateDescriptor(u32 offset, u16 selector, GateType type, PrivilegeLevel ring) {
			this->offset_0 = static_cast<u16>(offset & 0b00000000000000001111111111111111);
			this->selector = selector;
			this->reserved = 0;
			this->type = type;
			this->zero = false;
			this->privilege_level = ring;
			this->present = true;
			this->offset_1 = static_cast<u16>((offset & 0b11111111111111110000000000000000) >> 16);
		}
	} __attribute__((packed));
}
