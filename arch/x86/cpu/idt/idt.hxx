/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <cpu/idt/gate/descriptor.hxx>
#include <cpu/gdt/segment/selector.hxx>

#define MAX_IDT_ENTRIES 256

namespace inanis::arch::cpu::idt {

	struct IDT {
		__attribute__((aligned(0x10)))
		GateDescriptor entries[MAX_IDT_ENTRIES];
	} __attribute__((packed));

	struct IDTR {
		u16 size : 16;
		u32 offset : 32;
	} __attribute__((packed));

	void initialize();
}
