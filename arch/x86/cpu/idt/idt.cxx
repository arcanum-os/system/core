/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "idt.hxx"
#include <cpu/exceptions/exceptions.hxx>

namespace inanis::arch::cpu::idt {

	void initialize() {
		static IDT idt;
		static IDTR idtr;

		exceptions::initialize(idt.entries);

		idtr.size = (sizeof(GateDescriptor) * MAX_IDT_ENTRIES) - 1;
		idtr.offset = (u32) &idt.entries[0];

		asm volatile("lidt %0" : : "m"(idtr));
		asm volatile("sti");
	}
}
