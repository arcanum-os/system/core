/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <types.h>

namespace inanis::arch::cpu {

	enum PrivilegeLevel : u8 {
		KernelMode = 0,
		Ring1 = 1,
		Ring2 = 2,
		UserMode = 3,
	};

	void initialize();
}
