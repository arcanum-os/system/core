; This Source Code Form is subject to the terms of the Mozilla Public
; License, v. 2.0. If a copy of the MPL was not distributed with this
; file, You can obtain one at http://mozilla.org/MPL/2.0/.

section .text
global _load_gdt:function
global _reload_segments:function

_load_gdt:
	mov eax, [esp + 4]
	lgdt [eax]
	ret

_reload_segments:
	; Reload CS register containing code selector:
	jmp 0x08:.reload_cs ; 0x08 is a stand-in for your code segment
.reload_cs:
	; Reload data segment registers:
	mov ax, 0x10 ; 0x10 is a stand-in for your data segment
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	ret
