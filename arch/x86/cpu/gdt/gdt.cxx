/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "gdt.hxx"

namespace inanis::arch::cpu::gdt {

	void initialize() {
		static GDT gdt;
		static GDTR gdtr;

		static SegmentDescriptor null = gdt::SegmentDescriptor(0, 0, 0, 0);
		static SegmentDescriptor kernel_code = gdt::SegmentDescriptor(0, 0xFFFFF, 0x9A, 0xC);
		static SegmentDescriptor kernel_data = gdt::SegmentDescriptor(0, 0xFFFFF, 0x92, 0xC);

		gdt.entries[0] = null;
		gdt.entries[1] = kernel_code;
		gdt.entries[2] = kernel_data;

		gdtr.size = (3 * sizeof(SegmentDescriptor)) - 1;
		gdtr.offset = (u32) &gdt;

		_load_gdt((u32)(&gdtr));
		_reload_segments();
	}
}
