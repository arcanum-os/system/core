/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <cpu/gdt/segment/descriptor.hxx>

#define MAX_GDT_ENTRIES 8

extern "C" void _load_gdt(u32 gdtr);
extern "C" void _reload_segments();

namespace inanis::arch::cpu::gdt {

	struct GDT {
		__attribute__((aligned(0x10)))
		SegmentDescriptor entries[MAX_GDT_ENTRIES];
	} __attribute__((packed));

	struct GDTR {
		u16 size : 16;
		u32 offset : 32;
	} __attribute__((packed));

	void initialize();
}
