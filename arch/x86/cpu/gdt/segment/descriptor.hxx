/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <types.h>

namespace inanis::arch::cpu::gdt {

	struct SegmentDescriptor {
		u16 limit_0 : 16;
		u32 base_0 : 24;
		u8 access_byte : 8;
		u8 limit_1 : 4;
		u8 flags : 4;
		u8 base_1 : 8;

		SegmentDescriptor() { }

		SegmentDescriptor(u32 base, u32 limit, u8 access_byte, u8 flags) {
			this->limit_0 = static_cast<u16>(limit & 0b00000000000000001111111111111111);
			this->base_0 = static_cast<u32>(base & 0b00000000111111111111111111111111);
			this->access_byte = access_byte;
			this->limit_1 = static_cast<u8>((limit & 0b00000000000011110000000000000000) >> 16);
			this->flags = static_cast<u8>(flags & 0b00001111);
			this->base_1 = static_cast<u8>((base & 0b11111111000000000000000000000000) >> 24);
		}
	} __attribute__((packed));
}
