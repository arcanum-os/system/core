/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <types.h>
#include <cpu/cpu.hxx>

namespace inanis::arch::cpu::gdt {

	struct SegmentSelector {
		u8 rpl : 2;
		bool ti : 1;
		u16 index : 13;

		SegmentSelector(PrivilegeLevel ring, bool ti, u16 index) {
			this->rpl = ring;
			this->ti = ti;
			this->index = (index >> 2);
		}
	} __attribute__((packed));
}
