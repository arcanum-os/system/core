/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#pragma once

#include <cpu/idt/gate/descriptor.hxx>

namespace inanis::arch::cpu::exceptions {

	struct interrupt_frame {
		u32 ip;
		u32 cs;
		u32 flags;
		u32 sp;
		u32 ss;
	};

	void set_exception_handler(idt::GateDescriptor* idt, size_t vector,
		void (*handler)(interrupt_frame*, u32));
	
	void initialize(idt::GateDescriptor* idt);
}
