/*
	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

#include "exceptions.hxx"
#include <logging/logging.hxx>
#include <panic.hxx>
#include <stdio.h>

namespace inanis::arch::cpu::exceptions {

	__attribute__((interrupt))
	extern "C" void generic_exception_handler(interrupt_frame* frame, u32 code) {
		LOG(FATAL, "generic exception handler hit! panicking");
		PANIC("CPU exception caught");
	}

	void set_exception_handler(idt::GateDescriptor* idt, size_t vector,
		void (*handler)(interrupt_frame*, u32)) {

		idt[vector] = idt::GateDescriptor((u32) handler, 0x08,
			idt::GateType::InterruptGate32, PrivilegeLevel::KernelMode);
	}

	void initialize(idt::GateDescriptor* idt) {
		for(u8 v = 0; v < 32; v++) {
			set_exception_handler(idt, v, generic_exception_handler);
		}
	}
}
