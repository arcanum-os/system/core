; This Source Code Form is subject to the terms of the Mozilla Public
; License, v. 2.0. If a copy of the MPL was not distributed with this
; file, You can obtain one at http://mozilla.org/MPL/2.0/.

extern phys_kernel_start
extern phys_kernel_end
extern virt_kernel_start
extern virt_kernel_end

section .bss
	align 4096
boot_page_directory:
	resb 4096
boot_page_table1:
	resb 4096

section .stack
stack_bottom:
	resb 16384
stack_top:

section .stage2.data
	bootinfo resb 3 * 4

section .stage1.text
global _start
_start:
	mov edi, boot_page_table1 - 0xC0000000
	mov esi, 0
	mov ecx, 1023
.1:
	cmp dword esi, 0x100000
	jl .2
	cmp dword esi, virt_kernel_end - 0xC0000000
	jge .3

	mov edx, esi
	or edx, 0x003
	mov [edi], edx
.2:
	add esi, 0x1000 ; page size
	add edi, 4

	loop .1
.3:
	; identity map
	mov dword [boot_page_directory - 0xC0000000 + 0], boot_page_table1 - 0xC0000000 + 0x003

	; higher half map
	mov dword [boot_page_directory - 0xC0000000 + 768 * 4], boot_page_table1 - 0xC0000000 + 0x003

	mov ecx, boot_page_directory - 0xC0000000
	mov cr3, ecx

	mov ecx, cr0
	or ecx, 0x80010000
	mov cr0, ecx

	lea ecx, _stage2
	jmp dword ecx

section .stage2.text
_stage2:
	mov dword [boot_page_directory + 0], 0

	mov ecx, cr3
	mov cr3, ecx

	mov esp, stack_top

	; fill in bootinfo struct
	mov byte [bootinfo], 3

	push dword bootinfo

	; call stage 3
	extern _start1
	call _start1

	cli
.1:
	hlt
	jmp .1
